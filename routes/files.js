var express = require('express');
var router = express.Router();

let awsS3 = require('../utils/awsS3');
let request = require('request');
let singleUpload = awsS3.upload();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index', {title: 'Express'});
});


router.post('/upload', function (req, res, next) {

    console.log('req.body.filePath=>', req.headers['content-type'])
    if (req.headers['content-type'].includes("multipart/form-data")) {

        singleUpload(req, res, function (err, result) {
            if (err) {
                res.status(422).send({errors: [{title: 'Image Upload Error', detail: err.message}]});
            }

            console.log('req.file=>', JSON.stringify(req.file))
            // let filePath = req.file.location;
            // let baseUrl = "/5000/debug/test";
            // let pathParams = "/filePath/:" + filePath;
            // let url = baseUrl + pathParams;
            //
            // request.get(url, function (err, res, body) {
            //     console.log('body=>', body)
            // })
            res.status(200).send({success: true})


        });
    } else {
        res.status(400).send({success: false})
    }
})
module.exports = router;
