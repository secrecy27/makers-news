var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
var articlesSchema = require('../models/articles');

module.exports = mongoose.model('Articles', articlesSchema);
