var express = require('express');
var router = express.Router();
const jsonfile = require('jsonfile')
var db = require('../utils/db');
var Articles = require('../libs/articles');
var Comments = require('../libs/comments');
const language = require('@google-cloud/language');
const path = require('path');

var service_key_file_name = '../blockFiles/daios-google-private.json';
const service_key_file = path.join(__dirname, service_key_file_name)
const client = new language.LanguageServiceClient({
    keyFilename: service_key_file
});

var fileName = '../blockFiles/makers_logs.json';
const file = path.join(__dirname, fileName)


/* GET users listing. */
router.get('/:articleId', function(req, res, next) {
    var id = req.params.articleId;
    db.connectDB()
        .then(() =>
            getArticleById(id)
                .then(result => {
                    console.log('result=>', result)
                    res.status(200).send(result);
                }))
});

router.post('/', function(req, res, next) {

    db.connectDB()
            .then(() =>
                createArticle(sampleJson)
                    .then(result => {
                        console.log('result=>', result)
                        res.status(200).send(result);
                    }))

});

router.post('/:articleId/comments', function (req, res , next) {
    var articleId = req.params.articleId;
    var body = req.body;
    console.log('articleId=>', articleId);
    console.log('body=>', body);

    const document = {
        content: body.message,
        type: 'PLAIN_TEXT',
    };

    client
        .analyzeSentiment({document: document})
        .then(results => {
            const sentiment = results[0].documentSentiment;

            console.log(`Text: ${body.message}`);
            console.log(`Sentiment score: ${sentiment.score}`);
            console.log(`Sentiment magnitude: ${sentiment.magnitude}`);

            body['score'] = sentiment.score;
            body['regDate'] = new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '');

            db.connectDB()
                .then(() => {
                    addArticleWithComment(articleId, body)
                        .then(result=> {
                            getArticleById(articleId)
                                .then(result => {
                                    var lastLength = result.comments.length-1;
                                    console.log('result=>', result.comments[lastLength])
                                    res.status(200).send(result.comments[lastLength]);
                                })
                        })
                })
        })
        .catch(err => {
            console.error('ERROR:', err);
        });

    jsonfile.readFile(file)
        .then((obj) => {

            obj.push(body)
            console.log('obj=>', obj);

            jsonfile.writeFile(file, obj, { spaces: 2, EOL: '\r\n' }, function (err) {
                if (err) {
                    console.error(err)
                    // res.status(500).send({success:"fail"})
                }
                // res.status(200).send({success:"ok"})
            })
        })
        .catch(err => {
            console.log('catch err=>', err);

            let obj = []
            obj.push(body)

            jsonfile.writeFile(file, obj, { spaces: 2, EOL: '\r\n' }, function (err) {
                if (err) {
                    console.error(err)
                    // res.status(500).send({success:"fail"})
                }
                // res.status(200).send({success:"ok"})
            })
        })

})

router.post('/:articleId/likes', function (req, res , next) {
    var articleId = req.params.articleId;

    db.connectDB()
        .then(() => updateArticleByIdWithLikes(articleId)
            .then(result => {
                console.log('likes=>', result['likes'])
                res.status(200).send({"likes":result['likes']})

                // jsonfile.readFile(file)
                //     .then((obj) => {
                //
                //         let body = {
                //             "articleId": articleId,
                //             "likes": result['likes']
                //         }
                //         obj.push(body)
                //         console.log('obj=>', obj);
                //
                //         jsonfile.writeFile(file, obj, { spaces: 2, EOL: '\r\n' }, function (err) {
                //             if (err) {
                //                 console.error(err)
                //                 // res.status(500).send({success:"fail"})
                //             }
                //             // res.status(200).send({success:"ok"})
                //         })
                //     })
                //     .catch(err => {
                //         console.log('catch err=>', err);
                //
                //         let obj = []
                //         let body = {
                //             "articleId": articleId,
                //             "likes": result['likes']
                //         }
                //
                //         obj.push(body)
                //
                //         jsonfile.writeFile(file, obj, { spaces: 2, EOL: '\r\n' }, function (err) {
                //             if (err) {
                //                 console.error(err)
                //                 // res.status(500).send({success:"fail"})
                //             }
                //             // res.status(200).send({success:"ok"})
                //         })
                //     })

            }))
});
function createComment (data) {
    return new Promise((resolve, reject) => {
        console.log(data)

        var comments = new Comments(data)
        comments.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createComments done: ' + result)
                resolve(result)
            }
        })
    })
}

function createArticle(data) {
    return new Promise((resolve, reject) => {
        console.log(data)

        var articles = new Articles(data)
        articles.save(function (err, result) {
            if (err) {
                reject(err);
            } else {
                console.log('createArticles done: ' + result)
                resolve(result)
            }
        })
    })
}

function getArticleById (id) {
    return new Promise((resolve, reject) => {
        Articles.findOne(
            {"id": id},
            function(err, article) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                console.log('getArticleById done: ' + article)
                resolve(article)
            }
        )
    })
}

function addArticleWithComment(articleId, comment) {
    return new Promise((resolve, reject) => {

        Articles.findOneAndUpdate(
            {
                "id": articleId
            },
            {
                $push: {comments: comment}
            },
            {upsert: false, new: false},
            function (err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

function updateArticleByIdWithLikes (articleId) {
    return new Promise((resolve, reject) => {
        Articles.findOneAndUpdate(
            {"id": articleId
            },
            {$inc: {likes : 1}
            },
            {upsert: false, new: true},
            function(err, data) {
                if (err) {
                    console.error(err)
                    reject(err)
                }
                resolve(data)
            })
    })
}

var sampleJson = {
    "id":"8",
    "title": "가격 변동성에도 암호화폐·블록체인 긍정적 시각 이어져…\"기관투자자 관심 높아\"",
    "content": "지난 24시간 동안 비트코인(BTC)을 비롯한 주요 암호화폐가 큰 가격 변동성을 보여주면서 투자자들이 불안해 하는 가운데, 여전히 상당수 전문가들은 암호화폐 시장에 대한 긍정적인 시선을 거두지 않고 있다.\n" +
        " \n" +
        "12일(현지시간) 암호화폐 전문매체 AMB크립토에 따르면 JP모건 에셋 매니지먼트 수석인 브라이슨 레이크(Bryson Lake)는 최근 블룸버그와의 인터뷰에서 \"암호화폐 시장이 안정기에 진입하면서 투자자도 적극적인 반응을 보이는 분위기\" 라며 \"특히 최근에는 기관 투자자도 암호화폐에 높은 관심을 보이고 있다\"고 강조했다.\n" +
        " \n" +
        "또 10일 미국 경제 매체 CNBC에 따르면, 유나이티드 캐피탈(United Capital)의 조 듀란(Joe Duran) CEO는 \"블록체인에 대해 긍정적으로 생각한다\"고 밝혔다. 그는 \"향후 블록체인은 세계에서 발생하는 모든 거래에 관여하게 될 것\"이라며 \"다만, 그러기 위해서는 약 10년의 시간이 필요할 것으로 보인다\"고 덧붙였다. 또한 그는 \"10년 이후, 블록체인은 현재의 인터넷과 같이 매우 중요한 역할을 할 것이다\"고 내다봤다.\n" +
        " \n" +
        "앞서 지난 7일 CNBC는 김용 세계은행 총재와의 인터뷰를 통해 비트코인 등 암호화폐를 \"폰지 사기'로 규정하면서도  \"블록체인 기술을 진짜로 신중하게 보고 있다. 이 기술이 개발도상국에서 돈을 보다 효과적으로 추적할 수있으며 부패를 줄이는데 사용될 수 있다는 기대가 있다\"고 전했다.\n" +
        " \n" +
        "유명 암호화폐 애널리스트 조셉 영(Joseph Young)도 지난 10일 자신의 트위터를 통해 \"암호화폐는 결제 수단의 일종이 될 수 있다고 생각한다. 비트코인(BTC)은 신용카드 등 전통 금융 시스템의 대안이 될 수 있다. 암호화폐 결제 수단 채택 가맹점 및 소비자가 늘어나는 것은 업계에 긍정적으로 작용할 것\"이라고 밝혔다.\n" +
        " \n" +
        "한편 이날 암호화폐 전문 미디어 지크립토(Zycrypto)에 따르면, 우크라이나 블록체인 협회 마이클 초바니안(Michael Chobanian) 협회장은 \"암호화폐를 통해 국가의 안정과 독립을 도모할 것\"이라고 밝혔다. 그는 \"현재 우크라이나는 소련 붕괴 초기 당시만큼 위험한 상태다\"며 \"비트코인(BTC)과 기타 암호화폐는 자유와 새로운 기점이 될 것이다\"고 덧붙였다.",
    "writer": "박병화 desk@coinreaders.com",
    "regDate": new Date().toLocaleString('ko-KR').replace(/T/, ' ').replace(/\..+/, '')
}

module.exports = router;
