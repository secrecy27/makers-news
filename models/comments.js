var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId;

var commentsSchema = new Schema({
    name : String,
    email: String,
    message: String,
    like: Number,
    score: Number,
    address: String,
    regDate: String
});

module.exports = commentsSchema;
